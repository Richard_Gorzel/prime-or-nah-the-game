# Prime or Nah the Game

A quick game to see how good you are at identifying primes. You can set the generator to choose a random number as large as python allows, but it is capped at 10,000 by default.

You will need python3 and tkinter to run this project.

Run the game using `python3 "path\to\game"` in the terminal.
