#!python3

# gives you a number, then you select prime or nah and it tells you if you're right
# score is kept

# library for user interface
import tkinter as tk
from random import randint as random
from math import sqrt

#create the main window
window = tk.Tk()
window.title("Prime or Nah?")

#global variables
MaxNum = 10000

NumbersGuessed = 0
PrimesGuessed = 0
NumbersIdentified = 0
PrimesIdentified = 0
Num = 0

#functions
def reroll():
	global Num
	global MaxNum
	Num = random(1,MaxNum)
	NumberBox.config(text = str(int(Num)))
	pass

def updateUI():
	global NumbersGuessed
	global NumbersIdentified
	global PrimesGuessed
	global PrimesIdentified
	NumberBox.config(text = int(Num))
	if (NumbersGuessed > 0):
		ScoreBox.config(text = str(NumbersIdentified) + "/" + str(NumbersGuessed) + " (" + str(int(100 * NumbersIdentified / NumbersGuessed)) + "%)")
	else:
		ScoreBox.config(text = "0/0 (100%)")
	if (NumbersGuessed-PrimesGuessed) > 0:
		NonPrimeScoreBox.config(text = str((NumbersIdentified-PrimesIdentified)) + "/" + str((NumbersGuessed-PrimesGuessed)) + " (" + str(int(100 * (NumbersIdentified-PrimesIdentified) / (NumbersGuessed-PrimesGuessed))) + "%)")
	else:
		NonPrimeScoreBox.config(text = "0/0 (100%)")
	if (PrimesGuessed > 0):
		PrimeScoreBox.config(text = str(PrimesIdentified) + "/" + str(PrimesGuessed) + " (" + str(int(100 * PrimesIdentified / PrimesGuessed)) + "%)")
	else:
		PrimeScoreBox.config(text = "0/0 (100%)")

def isPrime():
	global Num
	if (Num <= 6):
		if Num == 1 or Num == 4 or Num == 6:
			print(str(Num) + ": Not Prime")
			return False
	elif ((Num % 6) == 1) or ((Num % 6) == 5):
		UpperBound = int(sqrt(Num) + 1)
		LowerBound = 5
		for i in range(LowerBound, UpperBound, 2):
			if Num % i == 0:
				print(str(Num) + ": Not Prime - Divisible By: " + str(i))
				return False
	else:
		print(str(Num) + ": Not Prime")
		return False
	print(str(Num) + ": Prime")
	return True

def prime():
	global NumbersGuessed
	global NumbersIdentified
	global PrimesGuessed
	global PrimesIdentified
	global Num
	NumbersGuessed += 1
	if isPrime():
		NumbersIdentified += 1
		PrimesGuessed += 1
		PrimesIdentified += 1
	reroll()
	updateUI()
	pass

def nah():
	global NumbersGuessed
	global NumbersIdentified
	global PrimesGuessed
	global PrimesIdentified
	global Num
	NumbersGuessed += 1
	if isPrime():
		PrimesGuessed += 1
	else:
		NumbersIdentified += 1
	reroll()
	updateUI()
	pass


#create the elements for the window
NumberLabel = tk.Label(text = "Is this prime?")
NumberBox = tk.Label(text = "")

ScoreLabel = tk.Label(text = "Score:")
ScoreBox = tk.Label(text = "")

PrimeScoreLabel = tk.Label(text = "Prime Score:")
PrimeScoreBox = tk.Label(text = "")

NonPrimeScoreLabel = tk.Label(text = "Non-Prime Score:")
NonPrimeScoreBox = tk.Label(text = "")

PrimeButton = tk.Button(text = "Prime", command=prime)
NahButton = tk.Button(text = "Nah", command=nah)

#arrange them in the window
ScoreLabel.grid(row=1, column=1)
ScoreBox.grid(row=1, column=2)

PrimeScoreLabel.grid(row=1,column=3)
PrimeScoreBox.grid(row=1, column=4)

NonPrimeScoreLabel.grid(row=1, column=5)
NonPrimeScoreBox.grid(row=1, column=6)

NumberLabel.grid(row=2, column=3, padx=5)
NumberBox.grid(row=2, column=4, padx=5)

PrimeButton.grid(row = 3, column = 1, columnspan = 3)
NahButton.grid(row = 3, column = 4, columnspan = 3)

#RunButton.grid(row=3, column=1, columnspan=2)











###############################################################################
#	Start the main program
###############################################################################
reroll()
updateUI()
window.mainloop()
